﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTry {
    class Cat {
        String name;
        int age;
        String owner;

        // Constructor 建構子
        public Cat(String name, int age) {
            this.name = name;    // 把Name 放進去外面的那個 name
            this.age = age;    // 把 age 放進去外面的那個 age
        }

        // Overloading， 可以建立多種不同的 Construcor(建構子)，我們叫做 Overloading
        public Cat(String name, int age, String owner) {
            this.name = name;
            this.age = age;
            this.owner = owner;    // 把 Owner 放入外面的那個 Owner 變數
        }

        // Method
        public String getCatsName() {
            return "他的名字是:" + this.name;
        }

        public String getCatsInt() {
            String age = this.age.ToString();
            return "他的年齡是:" + age;
        }

        public String getOwner() {
            return "他的主人是:" + this.owner;
        }

        public void changeOwnerToCool() {
            this.owner = "Cool";
        }

    }
}
