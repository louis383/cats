﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTry {
    class Program {
        static void Main(string[] args) {
            Cat cat = new Cat("Billy", 12);    // Constructor of cats
            Console.WriteLine( cat.getCatsName() );    // 印出名字
            Console.WriteLine( cat.getCatsInt() );     // 印出年齡
            Console.ReadLine();    // 停下終端機
        }
    }
}
